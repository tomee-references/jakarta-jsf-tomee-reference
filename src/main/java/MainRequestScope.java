import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Named
@RequestScoped
public class MainRequestScope extends MainAbstractExample {

    private static Logger logger = LogManager.getLogger();
    @PostConstruct
    public void init() {
        super.init();
    }

    @PreDestroy
    public void destroy(){
        super.destruct(getClass().getName());
    }

}
