import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.Serializable;

@Named
@SessionScoped
public class MainSessionScope extends MainAbstractExample implements Serializable {

    private static Logger logger = LogManager.getLogger();
    @PostConstruct
    public void init() {
        super.init();
    }

    @PreDestroy
    public void destroy(){
        super.destruct(getClass().getName());
    }
}
