import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class MainAbstractExample {

    private static Logger logger = LogManager.getLogger();
    private int counter;

    public void init() {
        counter = 0;
    }

    public void destruct(String className) {
        logger.info(className + " was shutdonw");
    }

    public int getCounter() {
        counter++;
        logger.info("Counter wurde betätigt");
        return counter;
    }
}
