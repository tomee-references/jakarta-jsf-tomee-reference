# TomEE Jsf Basic Setup

## What is this Project for?

This Project shows a basic setup for a TomEE Server which runs a full fetchted Jakarta Stack to provide a JSF Website. This Project is only intended to provide a barebowns setup which can be used as a Starter for new Projects or as a Reference for TomEE JSF projects.

I wanted a reference which i can use as a Starter for TomEE as a modern JSF setup with Jakarta. The Idea is to use ths Project as a Starter like a SpringBoot starter so I don't have to create the project from scratch 

## Setup
You need Java 11 or higher. Download the latest TomEE Server from the [official Page](https://tomee.apache.org/download.html). I use the Webprofile-ZIP version.

#### Intellij Ultimate
I use the run configuration from Intellij-Ultimate which supports TomEE. Simply add a new configuration add you TomEE Server and add a .war Artifact in the Deployment tab. 

There is currently a Bug in the Run-Configuration for TomEE which affects the Application Context. This leads to the issue, that Context is not set while Startup. The TomEE will set the context which makes it look like the page is not reachable. If you don't find or know you Page after Startup go to you TomEE Manager. This lists the running Applications with the correct Context. You can set a user in the conf folder in the tomcat-users.xml. This Issue is reported to JetBrains in the Issues [IDEA-291278](https://youtrack.jetbrains.com/issue/IDEA-291278) [IDEA-261464](https://youtrack.jetbrains.com/issue/IDEA-261464)

## Dependencies
The project is set up in Gradle. In the build.gradle are three nassacary dependencies listed, which are needed for this setup. 

First the **Jakarta.platform** is needed. This is referenced as providedCompile, because the TomEE Server will provide all Libraries which can be found in the libs directory. TomEE provides this libs so we don't need to take care of them. If the application needs to run on a barebones Tomcat we would need to import all dependencies in the war. 
```gradle
    // https://mvnrepository.com/artifact/jakarta.platform/jakarta.jakartaee-web-api
    providedCompile group: 'jakarta.platform', name: 'jakarta.jakartaee-web-api', version: '9.1.0'
```

Unfortunatly the JSF Libraries are not included in TomEE by time of writing. So we need to import them in the Project manually.
```gradle
    // https://mvnrepository.com/artifact/org.apache.myfaces.core/myfaces-api
    implementation group: 'org.apache.myfaces.core', name: 'myfaces-api', version: '3.0.1'

    // https://mvnrepository.com/artifact/org.apache.myfaces.core/myfaces-impl
    implementation group: 'org.apache.myfaces.core', name: 'myfaces-impl', version: '3.0.1'
```

I also included `Primefaces` and `Log4j` because i use them all the time but technically they are not mandatory.

## Namespace
The whole project is setup in the Jakarta namespace. Not only the Namespace in the Classes but also the references that are needed in pages and configs.

#### web.xml
```xml
<web-app version="5.0"
         xmlns="https://jakarta.ee/xml/ns/jakartaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="https://jakarta.ee/xml/ns/jakartaee https://jakarta.ee/xml/ns/jakartaee/web-app_5_0.xsd">
```

#### beans.xml
```xml
<beans xmlns="https://jakarta.ee/xml/ns/jakartaee"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="https://jakarta.ee/xml/ns/jakartaee https://jakarta.ee/xml/ns/jakartaee/beans_3_0.xsd"
       bean-discovery-mode="all">
</beans>
```

#### faces-config.xml
```xml
<faces-config version="3.0" xmlns="https://jakarta.ee/xml/ns/jakartaee"
              xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
              xsi:schemaLocation="https://jakarta.ee/xml/ns/jakartaee
              https://jakarta.ee/xml/ns/jakartaee/web-facesconfig_3_0.xsd">

</faces-config>
```